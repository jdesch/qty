# Qty

**Qty** is a Python library for dealing with physical quantities.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install **Qty**.

```
pip install qty
```

## Usage

```python
from qty import Energy

>>> E = Energy()
>>> E.meV = 1000.
>>> E.J
1.602176634e-19
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)

