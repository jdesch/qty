Tutorial
========

This tutorial will guide you through the basics of **Qty**.

Installation
------------

Please refer to the :ref:`Installation` section.

Importing
---------

Import the relevant classes from the **Qty** module. This tutorial will use Energy quantities.

.. code-block:: python

   >>> from qty import Energy

Defining a Quantity
-------------------

A quantity is an instance of one of the classes inheriting from the :class:`qty.Quantity` class. To define a quantity, simply instantiate the appropriate class.

.. code-block:: python

   >>> quantity = Energy()

Quantities implement a set of descriptors that are used to store and retrieve their magnitudes. The setter converts and stores the quantity to the base units of the class. For example, the base units of the :class:`qty.Energy` class are joules (J). The getter converts and returns the quantity in the desired units.

.. code-block:: python

   >>> quantity.meV = 1600.
   >>> quantity.nm
   774.9012402075016

.. note::
   The units are implemented as descriptors and are therefore assigned and accessed without arguments.

Operations on Quantities
------------------------

**Qty** defines classes that hold quantities of a common type, in a wealth of `units of measurement`_. Unless dimensionless, a quantity has both a magnitude and a unit.

.. _units of measurement: https://en.wikipedia.org/wiki/Unit_of_measurement

=========== ============
Method      Expression
=========== ============
__add__     ``q1 + q2``
__eq__      ``q1 == q2``
__le__      ``q1 <= q2``
__lt__      ``q1 < q2``
__mul__     ``q * 2.``
__neg__     ``-q``
__pos__     ``+q``
__rmul__    ``2. * q``
__truediv__ ``q / 2.``
=========== ============
   
