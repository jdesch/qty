Installation
============

Use the package manager `pip`_ to install **Qty**.

.. code-block:: bash

   pip install qty

.. _pip: https://pip.pypa.io/en/stable/

