Qty Documentation
========================

`Qty`_ is a Python library for dealing with physical quantities.

.. _Qty: https://gitlab.com/jdesch/qty

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   tutorial


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
